Eines der ersten Experimente, das bei FLUTE durchgeführt wird, ist die longitudinale Bunchdiagnose mit einem Split-Ring Resonator. Bei diesem Streakingexperiment werden die Bunche durch ein sich schnell änderndes elektrisches Feld so abgelenkt, dass ihre zeitliche Ladungsdichte untersucht werden kann; ein wichtiger Parameter in der Optimierung von Beschleunigern. Mit dem SRR sind theoretisch Auflösungen im Subfemtosekundenbereich möglich \cite{2}. Als Quelle des sich schnell ändernden elektrischen Feldes wird THz-Strahlung genutzt, welche vom Laser erzeugt wird. Mit dem dafür konzipierten Aufbau befasst sich dieser Teil der Arbeit. 

\section{Funktionsweise des Split-Ring Resonators}

Im Kapitel 2 dieser Arbeit wird die Aufgabe des Lasers, Bunche zu erzeugen, behandelt. Ein Teil des Lasers wird davor für die Erzeugung von THz-Pulsen genutzt. Diese THz-Pulse koppeln dann an einen Ring mit einem kleinen Spalt. Der Ring fungiert quasi als Antenne und im Spalt wird ein starkes, schnell oszillierendes elektrisches Feld angeregt. Synchronisiert man dieses mit dem Durchflug eines Bunches durch den Spalt, lassen sich die Elektronen im Bunch durch das sich ändernde E-Feld kontrolliert transversal ablenken (siehe Abb. \ref{fig:srr}). Aus der zeitlichen Information (entlang der Flugrichtung der Bunche) wird eine räumliche, die dann auf einem Schirm abgelesen werden kann \cite{2}.

\begin{figure}[h!]
	\includegraphics[scale=0.5]{Bilder/srr-visualisation.PNG}
	\caption{Schematische Darstellung des SRR Experiments. Die drei Pfeile in rot, grün und blau bezeichnen die Flugbahnen der Elektronen eines Bunches. Die roten Kugeln sind Elektronen im vorderen Teil, die grünen Kugeln Elektronen im mittleren Teil und die blauen Kugeln Elektronen im hinteren Teil des Bunches. Durch die schnelle Änderung des elektrischen Feldes im Spalt des SRR ist die Ablenkung der Elektronen im vorderen Teil des Bunches eine andere als die der im hinteren Teil. Auf dem Schirm ist dann das longitudinale Bunchprofil zu sehen. Zeichnung nach \cite{2}.}
	\label{fig:srr}
\end{figure}

\clearpage

\section{Prinzip der THz-Erzeugung}

\begin{wrapfigure}{r}{8cm}
	\includegraphics[scale=0.6]{Bilder/tilted-pulse-front.png}
	\caption{Neigung der Pulsfront mit Hilfe einer Gitters. Entscheidend ist, dass der reflektierte Puls nicht die 0-te Ordnung der Beugung ist. Je nach Winkel des Gitters kann eingestellt werden, in welche Ordnung ein Großteil der Intensität reflektiert wird \cite{6}.}
	\label{fig:tilted}
\end{wrapfigure}

Wie in Abb.\ref{fig:srr} gezeigt, ist für das SRR-Experiment THz-Strahlung nötig. Um diese zu erzeugen, wird ein kurzer 800 nm Laserpuls an einem Gitter gebeugt und auf einen LiNbO$_3$ Kristall reflektiert. Über den nichtlinearen Effekt der optischen Gleichrichtung (engl. optical rectification - OR) entsteht im Kristall dann THz-Strahlung. Die Effizienz dieses Prozesses hängt empfindlichst von der Erfüllung der Geschwindigkeitsanpassungsbedingung zwischen der Gruppengeschwindigkeit des Infrarotpulses und der Phasengeschwindigkeit der THz-Strahlung
\begin{equation}
v^{gr}_{IR}=v^{ph}_{THz}
\end{equation}
ab \cite{6}.

Die hier verwendete Methode zur Erzeugung von OR im Kristall ist das Tilted Pulse-Front Pumping (TPFP), da damit hohe THz-Pulsenergien erreicht werden können \cite{18}. Das Gitter wird verwendet um die Pulsfront des Lasers um den Winkel $\gamma$ relativ zur Ausbreitungsrichtung zu kippen (siehe Abb.\ref{fig:tilted}). Dies ist nötig um die Geschwindigkeitsanpassung zu erreichen. Da sich die entstehende THz-Strahlung gemäß dem Huygensschen Prinzip senkrecht zur geneigten Pulsfront ausbreitet, liegt nun der Winkel $90\degree-\gamma$ zwischen den Ausbreitungsrichtungen von Laserpuls und THz. Durch diese Neigung enthält die Geschwindigkeitsanpassungsbedingung
\begin{equation}
	v^{gr}_{IR} *\cos(\gamma)=v^{ph}_{THz}
\end{equation}
nun mit $\gamma$ eine einstellbare Größe. Mit einem passend gewählten Gitter kann diese nun erfüllt und OR erreicht werden \cite{6}.

\section{Aufbau zur THz-Erzeugung}

Basierend auf Plänen von Dr. Zoltan Ollmann wurde ein Aufbau wie in Abb.\ref{fig:thzsetupandi} realisiert. Eine schematische Darstellung des originalen Aufbaus befindet sich im Anhang.

\begin{figure}[h!]
	\includegraphics[scale=0.6]{Bilder/thz-aufbau-andi.PNG}
	\caption{Schematische Darstellung des realisierten THz-Aufbaus. Nach der Kompression wird der Laserpuls am Gitter G1 gebeugt und reflektiert. Dies führt zur Neigung der Pulsfront, wie in Abb. \ref{fig:tilted} beschrieben. Das 4f-Abbildungssystem aus ZL1 und ZL2 bildet den Quellpunkt auf vom Gitter in den Kristall ab. Außerdem kompensiert es die mit der Beugung einhergehende Divergenz. Alle Elemente im blauen Rahmen sind zusammen das THz-Modul und sollen auf einer oder zwei optischen Platten aufgebaut werden, damit sie sich nicht relativ zueinander verschieben. So kann das Modul zuerst im Laserlabor aufgebaut und getestet werden und dann in die Experimentierhalle verbracht werden ohne es abbauen zu müssen. Die minimalen Abmessungen dieses Moduls sind \textbf{150 x 30 cm}. Eine tabellarische Auflistung aller verwendeten Elemente und ihrer Eigenschaften befindet sich im Anhang.}
	\label{fig:thzsetupandi}
\end{figure}

S1 bis S4 bezeichnen dielektrische Spiegel für 800 nm, wobei S4 aus dem Strahl herausgeklappt werden kann. I1 und I2 sind Irisblenden. Da die Winkel nach S4 sehr genau eingestellt werden müssen, ist es wichtig, einen verlässlichen, geraden Strahlverlauf zwischen S3 und S4 zu haben. Dafür wird S4 temporär weggeklappt und der Strahl mit I1 und I2 justiert. Mit den $\lambda/2$-Wellenplatten HWP1 und HWP2 wird die Polarisation des Laserstrahls eingestellt. Mit HWP1 wird sie nach dem Kompressor für das Gitter G1 auf horizontal gestellt, für den Kristall mit HWP2 wieder auf vertikal. CL1 und CL2 sind zylindrische Linsen, die mit den Brennweiten $f_1=\SI{421}{mm}$ und $f_2=\SI{250}{mm}$ ein 4f-Abbildungssystem bilden. So wird der geneigte Laserpuls in G1 scharf in den Kristall abgebildet.

Bei diesem Aufbau lag stets auch ein Augenmerk auf der Möglichkeit, ihn in Gänze zu bewegen. Da die THz-Ausbeute stark von der genauen Einstellung der Abstände und Winkel des Aufbaus abhängt, müssen die einzelnen Optiken nicht relativ zueinander neu justiert werden, wenn das ganze Modul bewegt werden soll. Gerade weil der Winkel zwischen S4-G1 und G1-Kristall und Abstände von G1, CL1, CL2 und Kristall so wichtig sind, kann die Justierung stark vereinfacht werden, wenn die Elemente sich nicht relativ zueinander verschieben. I1 und I2 als fester Teil des Moduls erleichtern die Justage zusätzlich. Das ganze Modul soll daher auf einer eigenständigen optischen Platte aufgebaut werden. Die minimale Länge des Aufbaus ist die Strecke von G1 bis zum Kristall und beträgt 1,5 m. Neben der Länge des 4f-Systems sind darin auch die Abmessungen der Optischen Halterungen und ein paar Zentimeter Spielraum berücksichtigt. Die minimale Breite wird durch den Abstand der Achse G1-Kristall zur Achse I1-I2 bestimmt und auf 15 cm festgelegt. Berücksichtigt man auch hier die Abmessung der Optiken, die Dicke des Strahls zwischen CL1 und CL2, sowie einen kleinen Spielraum ist die minimale Breite des Aufbaus 20 cm. Das THz-Modul hat zusammengefasst also eine minimale Größe von \textbf{150 x 30 cm}. Eine tabellarische Auflistung aller verwendeten Elemente und ihrer Eigenschaften befindet sich im Anhang.

\clearpage

\section{Ergebnisse und Diskussion}
Zum Nachweis der THz-Strahlung wurde in dieser Arbeit ein von der Physikalisch-Technischen Bundesanstalt (PTB) kalibrierter SLT THz 20 Pyrosensor verwendet. Er ist auf den Wellenlängenbereich von THz-Strahlung ausgelegt. Als zusätzlicher Filter wurde ein schwarzer Karton vor dem Sensor angebracht, der alle 800 nm Strahlung blockt. Über einen Verstärker mit einstellbarem Verstärkungsfaktor (x1, x10, x100 oder x1000) wurde dieser Sensor an ein Oszilloskop angeschlossen. Die Pulswiederholungsfrequenz von 1 kHz des Lasers (und damit auch der THz-Pulse) ist für den Sensor aber zu schnell und wirkt dort als quasikontinuierliches Signal. Da der Detektor nur Leistungsänderungen messen kann, wurde noch ein optischer Chopper direkt nach dem Kompressor eingebaut. Dabei handelt es sich um eine rotierende Scheibe, die den Strahl abwechselnd blockt bzw. durchlässt. Die verwendete Scheibe hatte zwei offene und zwei geschlossene Viertel und wurde mit 30 Hz rotiert. Daraus ergibt sich, dass der Sensor der Strahlung immer 8,3 ms ausgesetzt ist, gefolgt von 8,3 ms ohne Signal. Über den Zusammenhang
\begin{equation}
	P_{\text{THz}}=\frac{U_{\text{THz}}}{F*S}
\end{equation}
erhält man die Leistung der THz-Strahlung $P_{\text{THz}}$. Hierbei ist $U_{\text{THz}}$ die Höhe des gemessenen Peaks, $F$ der Verstärkungsfaktor und $S$ die Sensitivität des Detektors. Für $F$ wurde ein Wert von 100 gewählt. $S$ wurde vom PTB auf einen Wert von \SI{60,2}{\frac{V}{W}} kalibriert.. 
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.6]{Bilder/thzsignal.PNG}
	\caption{Gemessenes THz-Signal. Aufgrund der Chopperfrequenz von 30 Hz beträgt der Abstand zwischen zwei Peaks 16,7 ms. Mit Gleichung (3.3) und der Peakspannung von 250 mV kann die THz-Leistung berechnet werden. Sie beträgt in dieser Messung 42 $\mu$W. Bei derselben Eingangsleistung (2,8 W, vor dem Chopper) konnte im originalen THz-Aufbau eine THz-Leistung von etwa 640 $\mu$W erzielt werden. Weitere Optimierung ist daher notwendig.}
	\label{fig:firstthzpulse}
\end{figure}
In Abb.\ref{fig:firstthzpulse} ist das Signal der THz-Pulse, die mit diesem Aufbau erzeugt wurden, zu sehen. Die Peaks haben die erwartete Breite von 8,3 ms. Einsetzen der Peakspannung von 250 mV in Gleichung (3.3) ergibt eine THz-Leistung von 42 $\mu$W. Bei einer Eingangsleistung von 2.8 W bleibt dieser Wert weit hinter der mit dem originalen Aufbau gemessenen THz-Leistung von 640 $\mu$W zurück. Aus Zeitgründen konnte in dieser Arbeit aber keine weiteren Untersuchungen und Verbesserungen der Pulse vorgenommen werden. Die Funktionalität des Aufbaus ist hiermit aber dennoch prinzipiell gezeigt.
