\section{Pulsformen}
Wie bereits in Kapitel 2.3.1 beschrieben, ist die Pulslänge bei Korrelationsmessungen immer um einen Faktor kürzer als die Breite der ACF, da es sich im Wesentlichen um eine Faltung handelt. Je nach angenommener Pulsform variiert dieser Faktor. Der mathematische Zusammenhang soll im Folgenden kurz am Beispiel des gaußförmigen Pulses im Fall einer Intensitätsautokorrelation erläutert werden \cite{9}.

Ein Gaußpuls hat in seiner einfachsten Form die Gestalt
\begin{equation}
G(t)=e^{-t^2} .
\end{equation}
Aus Gleichung (2.1) ergibt sich die ACF zu:
\begin{equation}
G_{\text{ACF}}(\tau)=\int_{-\infty}^{\infty}G(t)G(t-\tau) dt=\sqrt{\frac{\pi}{2}}e^{-\frac{\tau^2}{2}} .
\end{equation}
Berechnet man nun die FWHM beider Ausdrücke, wobei A.2 noch mit einem Faktor multipliziert wird, der das Maximum gleich 1 setzt, erhält man:
\begin{eqnarray}
G(a)=\frac{1}{2}\Rightarrow a=\sqrt{\text{ln}(2)} ,\\
G_{\text{ACF}}(b)\sqrt{\frac{2}{\pi}}=\frac{1}{2}\Rightarrow b=\sqrt{2\text{ln}(2)} .
\end{eqnarray}
Der Quotient aus diesen beiden Werten ist der Transformationsfaktor zwischen ACF und Pulslänge:
\begin{equation}
	\frac{a}{b}=\frac{\sqrt{\text{ln}(2)}}{\sqrt{2\text{ln}(2)}}=\frac{1}{\sqrt{2}}.
\end{equation}

Analog dazu berechnen sich die Transformationsfaktoren für Sech$^2$

\begin{eqnarray}
	S(t)=\text{sech}(t)^2,\\[5pt]
	S_{\text{ACF}}(\tau)=\int_{-\infty}^{\infty}S(t)S(t-\tau) dt=4\text{csch}(\tau)^2(\tau \text{coth}(\tau)-1),\\
	\frac{a}{b}=\frac{1}{1,54},
\end{eqnarray}

und für Lorentzpulse:

\begin{eqnarray}
L(t)=\frac{1}{1+t^2},\\
L_{\text{ACF}}(\tau)=\int_{-\infty}^{\infty}L(t)L(t-\tau) dt=\frac{2\pi}{4+\tau^2},\\
\frac{a}{b}=\frac{1}{2}.
\end{eqnarray}
\clearpage
\section{Messgeräte und Messsoftware}

\begin{figure}[h!]
	\includegraphics[scale=0.13]{Bilder/LabMaxInLabEdit.png}
	\caption{Ein Aufbau zur Leistungsmessung. Der Messkopf ist nominell für Leistungen bis $\SI{20}{W}$ geeignet, aber es ist bei kurzen Pulsen häufig sinnvoll, solche Leistungsobergrenzen nicht auszureizen. Außerdem ist der Messkopf dem Laser bei Langzeitmessungen viele Stunden oder sogar Tage lang ausgesetzt. Daher wurde hier ein Leistungsabschwächer voran gestellt. }
	\label{fig:labmax}
\end{figure}

\begin{figure}[h!]
	\includegraphics[scale=0.12]{Bilder/MiniopticInLabEdit.png}
	\caption{Minioptic Delta Autokorrelator Messaufbau. Der Leistungsabschwächer reduziert die Leistung auf ca. 100 mW.}
	\label{fig:minioptic}
\end{figure}

\begin{figure}[h!]
	\includegraphics[scale=0.2]{Bilder/PulseCheckInLabEdit.png}
	\caption{APE PulseCheck Messaufbau. Im Bild ist das TPA-Modul eingebaut. Die maximale Laserleistung für dieses Gerät beträgt 15 mW.}
	\label{fig:pulsecheck}
\end{figure}

\begin{figure}[h!]
	\includegraphics[scale=0.15]{Bilder/miniTPAInLabEdit.png}
	\caption{APE miniTPA Messaufbau. Die maximale Laserleistung für dieses Gerät beträgt 15 mW.}
	\label{fig:minitpa}
\end{figure}

\begin{figure}[h!]
	\includegraphics[scale=0.7]{Bilder/9,8compos.PNG}
	\caption{Beispiel für eine Autokorrelationsmessung mit der Messsoftware PulseLink von APE, Berlin. Zu sehen ist die Messung in blau und der daraus berechnete Fit in rot (in diesem Fall ein Gaußfit). Der Wert 'ACF' ist die FWHM der Rohdaten. Der Wert 'Pulse fit' hingegen ist die FWHM aus den Fitparametern multipliziert mit dem pulsformabhängigen Faktor (hier $1/\sqrt{2}$ für gaußförmige Pulse).}
	\label{fig:pulselinkscreenshot}
\end{figure}

\clearpage
\section{Zusätzliche Informationen zum THz-Aufbau}
\begin{figure}[h!]
	\includegraphics[scale=0.4]{Bilder/thz-aufbau-zoltan.PNG}
	\caption{Originaler THz-Aufbau von Dr. Zoltan Ollmann.}
	\label{fig:thzsetupzoltan}
\end{figure}

\begin{table}[h!]
	\caption{Tabellarische Auflistung aller in Abb.\ref{fig:thzsetupandi} verwendeten Elemente.}
	\begin{tabular}{|c|c|c|}
		\hline
		Abkürzung&Element&Eigenschaften\\
		\hline\hline
		S1&Dielektrischer Spiegel&D=50,8 mm, HR=99,5\%, AOI=45\degree\\
		\hline
		S2&Dielektrischer Spiegel&D=50,8 mm, HR=99,5\%, AOI=45\degree\\
		\hline
		S3&Dielektrischer Spiegel&D=50,8 mm, HR=99,5\%, AOI=45\degree\\
		\hline
		S4&Dielektrischer Spiegel&D=50,8 mm, HR=99,5\%, AOI=0\degree\\
		\hline
		I1&Irisblende&1-25 mm Apertur\\
		\hline
		I2&Irisblende&1-25 mm Apertur\\
		\hline
		HWP1&$\lambda$/2 Wellenplatte&800 nm, 0-te Ordnung, 17 mm Apertur\\		
		\hline
		HWP2&$\lambda$/2 Wellenplatte&800 nm, 0-te Ordnung, 40 mm Apertur\\
		\hline
		G1&Reflektives Beugungsgitter&25x25 mm, 1800 Linien/mm\\
		\hline
		ZL1&Zylindrische Linse&Plankonvex, f=421 mm\\
		\hline
		ZL2&Zylindrische Linse&Plankonvex, f=250 mm\\
		\hline
		LiNbO$_3$ Kristall& Lithium Niobat Kristall&MgO dotiert\\
		\hline
	\end{tabular}
	\label{fig:thztable}
\end{table}